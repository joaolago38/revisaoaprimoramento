package br.srp_principio_responsabilidade_unica.violacao;

public class GerenciadorTarefas {

    public String conectarAPI(){
        //...
        return null;
    }
    public void criarTarefa(){
        //...
    }

    public void atualizarTarefa(){
        //...
    }

    public void removerTarefa(){
        //...
    }

    public void enviarNotificacao(){
        //...
    }

    public void produzirRelatorio(){
        //...
    }

    public void enviarRelatorio(){
        //...
    }
}
