package br.com.java.revisao.diverso.programas;

public class JavaSample1 {
    //    In Java, we can use String.valueOf() to convert a char array to a String.
    public static void main(String[] args) {
        char[] charArrays = new char[]{'1', '2', '3', 'A', 'B', 'C'};

        String str = new String(charArrays);
        System.out.println(str);    // 123ABC

        String str2 = String.valueOf(charArrays);
        System.out.println(str2);
    }
}
