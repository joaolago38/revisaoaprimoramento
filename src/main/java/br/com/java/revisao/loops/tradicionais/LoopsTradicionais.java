package br.com.java.revisao.loops.tradicionais;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class LoopsTradicionais {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4);
        for (Iterator<Integer> numero = lista.iterator(); numero.hasNext();) {
            Integer integer = numero.next();
            System.out.print(integer);
        }
    }
}
