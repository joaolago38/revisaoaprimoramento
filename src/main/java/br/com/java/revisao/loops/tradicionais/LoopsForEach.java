package br.com.java.revisao.loops.tradicionais;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class LoopsForEach {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4);
        for (Integer numero : lista) {
            System.out.print(numero);
        }
    }
}
