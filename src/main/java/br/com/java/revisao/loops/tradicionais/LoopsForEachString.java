package br.com.java.revisao.loops.tradicionais;

import java.util.Arrays;
import java.util.List;

public class LoopsForEachString {
    public static void main(String [] args){
        List<String> lista = Arrays.asList("\nBruna","\nBrena" , "\nBruno"
                , "\nSilvana", "\nEvaristo", "\nJose"
                , "\nMaria", "\nLeandro", "\nDaniela",
                "\nMaria Jose", "\nLenira", "\nIza", "\nBruna");
        for (String nome : lista) {
            System.out.print(nome);
        }
    }
}
