package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LoopsStreamsCollectorFilters {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 2, 3, 2, 1, 8, 5, 7, 4);
        Map<Boolean, List<Integer>> mapa = lista.stream()
                .map(e -> e * 2) // multiplica cada item por 2
                .collect(Collectors.groupingBy(e -> e > 8)); // agrupa itens baseado no resultado da comparação
        System.out.println(mapa);
    }
}
