package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class LoopsStreamsOperationEndsMax {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4);
        Optional<Integer> maiorNumero = lista.stream()
                .map(e -> e * 2) // multiplica cada item por 2
                .max(Comparator.naturalOrder()); // pega o maior item pela ordem natural
        System.out.println(maiorNumero.get());
    }
}
