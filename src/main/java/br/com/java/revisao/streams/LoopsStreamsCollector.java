package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LoopsStreamsCollector {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 2, 3, 2, 1, 8, 5, 7, 4);
        List<Integer> novaLista = lista.stream()
                .filter(e -> e % 2 == 0) // mantém apenas números pares
                .map(e -> e * 2) // multiplica cada item por 2
                .collect(Collectors.toList()); // coleta todos os itens em uma nova lista
        System.out.println(novaLista);
    }
}
