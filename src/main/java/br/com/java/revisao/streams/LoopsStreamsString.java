package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.List;

public class LoopsStreamsString {
    public static void main(String [] args){
        List<String> lista = Arrays.asList("\nBruna","\nBrena" , "\nBruno"
                , "\nSilvana", "\nEvaristo", "\nJose"
                , "\nMaria", "\nLeandro", "\nDaniela",
                "\nMaria Jose", "\nLenira", "\nIza", "\nBruna");
        lista.stream().forEach(System.out::print);
    }
}
