package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.List;

public class LoopsStreams {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4);
        lista.stream().forEach(System.out::print);
    }
}
