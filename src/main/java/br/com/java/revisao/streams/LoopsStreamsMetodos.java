package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.List;

public class LoopsStreamsMetodos {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4,11, 1,34,12,30,74,82,61,6,21,29,61,43,69, 95,78,30,4,12, 86,45,98,74, 2, 89, 11, 67,33,11, 48,75,
                11, 56, 58, 53, 60, 74, 17, 88, 62, 57, 53, 41,14, 2,63,62,21,80,19,19);
        lista.stream()
             .skip(2) // ignora os dois primeiros números
             .limit(9) // limita a 9 números
             .distinct() // ignora números iguais
             .forEach(System.out::print);
    }
}
