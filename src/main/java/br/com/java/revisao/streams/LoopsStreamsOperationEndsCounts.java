package br.com.java.revisao.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class LoopsStreamsOperationEndsCounts {
    public static void main(String [] args){
        List<Integer> lista = Arrays.asList(1, 5, 8, 7, 4, 6, 3, 2, 1, 8, 5, 7, 4);
        Long quantidade = lista.stream()
                .filter(e -> e % 2 == 0) // mantém apenas números pares
                .count(); // pega quantos itens restam no stream
        System.out.println(quantidade);
    }
}
