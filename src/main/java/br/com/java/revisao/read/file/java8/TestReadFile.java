package br.com.java.revisao.read.file.java8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class TestReadFile {
    public static void main(String args[]) {
        String fileName = "C:/Users/PC/projetos-estudos/revisao/revisaoaprimoramento/src/main/resources//arquivojava8.txt";

        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
